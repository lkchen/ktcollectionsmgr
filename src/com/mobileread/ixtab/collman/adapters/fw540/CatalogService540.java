package com.mobileread.ixtab.collman.adapters.fw540;

import java.util.Map;

import com.amazon.ebook.util.a.i;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.amazon.kindle.content.catalog.b;
import com.amazon.kindle.content.catalog.CatalogService.QueryResults;
import com.amazon.kindle.restricted.runtime.Framework;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;

public class CatalogService540 extends CatalogService {

	private final com.amazon.kindle.content.catalog.CatalogService delegate = (com.amazon.kindle.content.catalog.CatalogService) Framework
			.getService(com.amazon.kindle.content.catalog.CatalogService.class);

	public MutableEntry createMutableEntry(Object uuid) {
		return delegate.xG((i) uuid);
	}

	public MutableCollection createMutableCollection(Object uuid) {
		return delegate.CJ((i) uuid);
	}

	public Object createNewUUID() {
		return new i();
	}

	public CatalogTransaction openTransaction() {
		return new CatalogTransaction540(delegate.PH());
	}

	public CatalogEntry[] find(int context, Predicate predicate, int offset, int maxResults,
			final int[] totalCountPointer, QueryResultDepth depth) {
		// easy way to simulate a pointer using an array
		final CatalogEntry[][] raw = new CatalogEntry[][] { new CatalogEntry[0] };
		synchronized (raw) {
			if (delegate
					.YI((com.amazon.kindle.content.catalog.i) predicate.delegate,
							getCollation(context), maxResults, offset, new QueryResults() {

								public void bb(boolean flag, int i,
										int j, CatalogEntry[] acatalogentry,
										Map map) {
									if (totalCountPointer != null) {
										totalCountPointer[0] = i;
									}
									synchronized (raw) {
										raw[0] = acatalogentry;
										raw.notify();
									}
								}
							}, convert(depth))) {
				try {
					raw.wait();
				} catch (InterruptedException e) {
				}
				return raw[0];
			} else {
				return new CatalogEntry[0];
			}
		}
	}

	private com.amazon.kindle.content.catalog.I convert(
			QueryResultDepth depth) {
		return depth == QueryResultDepth.FAST ? com.amazon.kindle.content.catalog.I.e
				: com.amazon.kindle.content.catalog.I.B;
	}

	private b[] getCollation(int context) {
		return (b[]) Sort.getAsCriteria(this, context);
	}

	protected Object castToCollationCriteriaArray(Object[] criteria) {
		b[] result = new b[criteria.length];
		for (int i=0; i < criteria.length; ++i) {
			result[i] = (b) criteria[i];
		}
		return result;
	}

	protected Object createCollationCriteria(String text, boolean flag) {
		return new b(text, flag);
	}
}
