package com.mobileread.ixtab.collman;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import com.ibm.icu.text.MessageFormat;
import com.ibm.icu.util.ULocale;


public class I18n {
	
	private static final I18n instance = new I18n(getLocalizationURL());
	
	public static final String DISPLAY_COLLECTIONS_KEY = "display.collectionsonly";
	public static final String DISPLAY_COLLECTIONS_VALUE = "Display collections only";

	public static final String FILTER_LOWERONLY_KEY = "filter.loweronly";
	public static final String FILTER_LOWERONLY_VALUE = "Limit filters to the lower pane";
	
	public static final String INVISIBLE_SHOW_KEY = "invisible.show";
	public static final String INVISIBLE_SHOW_VALUE = "Show invisible items in Home";
	
	public static final String IMPORT_CALIBRE_KEY = "import.calibre";
	public static final String IMPORT_CALIBRE_VALUE = "Import collections from Calibre";
	public static final String EXPORT_CALIBRE_KEY = "export.calibre";
	public static final String EXPORT_CALIBRE_VALUE = "Export collections to Calibre";
	
	public static final String UPDATE_DIR_MENU_KEY = "update.dir.menu";
	public static final String UPDATE_DIR_MENU_VALUE = "Create collections from directories";
	public static final String UPDATE_DIR_MESSAGE_KEY = "update.dir.msg";
	public static final String UPDATE_DIR_MESSAGE_VALUE = "Attention: this is a potentially dangerous action. It will DELETE all collections on your device, and then create collections according to the directory structure of the documents/ folder.\n\nAre you sure you want to continue?";
	public static final String UPDATE_DIR_TITLE_KEY = "update.dir.title";
	public static final String UPDATE_DIR_TITLE_VALUE = "Import collections";
	
	public static final String DELETE_ALL_MENU_KEY = "delete.all.menu";
	public static final String DELETE_ALL_MENU_VALUE = "Delete all collections";
	public static final String DELETE_ALL_MESSAGE_KEY = "delete.all.msg";
	public static final String DELETE_ALL_MESSAGE_VALUE = "Are you sure you want to continue?";
	
	public static final String ACTION_DONE_KEY = "action.done";
	public static final String ACTION_DONE_VALUE = "Action completed successfully.";
	
	public static final String SHOW_UNFILED_ONLY_KEY = "show.unfiled.only";
	public static final String SHOW_UNFILED_ONLY_VALUE = "Upper panel: show unfiled items only";
	
	public static final String COLLECTION_NAME_KEY = "collection.name";
	public static final String COLLECTION_NAME_VALUE = "Collection Name";
	
	public static final String COLLECTION_EXISTS_KEY = "collection.exists";
	public static final String COLLECTION_EXISTS_VALUE = "A collection with that name already exists.";

	public static final String LOCALE_OVERRIDE = "locale.override";
	
	public static final String COLLECTIONCOUNT_ZERO_KEY = "collectioncount.zero";
	public static final String COLLECTIONCOUNT_ZERO_VALUE = "No items";
	
	public static final String COLLECTION_NONZERO_FORMAT_KEY = "collectioncount.nonzero.format";
	public static final String COLLECTION_NONZERO_FORMAT_VALUE = "{0,plural,one{# item}other{# items}}";
	
	public static final String AUTHOR_NONE_KEY = "author.none";
	public static final String AUTHOR_NONE_VALUE = "( no author )";
	
	public static final String MENU_MAIN_KEY = "menu.main";
	public static final String MENU_MAIN_VALUE = "Main Menu";
	
	public static final String MENU_DISPLAYOPTIONS_KEY = "menu.displayoptions";
	public static final String MENU_DISPLAYOPTIONS_VALUE = "Display Options";
	
	public static final String MENU_SORTOPTIONS_KEY = "menu.sortoptions";
	public static final String MENU_SORTOPTIONS_VALUE = "Sort Options";
	
	public static final String MENU_SYNC_KEY = "menu.sync";
	public static final String MENU_SYNC_VALUE = "Synchronization";
	
	public static final String SORT_TITLE_KEY = "sort.title";
	public static final String SORT_TITLE_VALUE = "Sort by: Title";
	
	public static final String SORT_TITLE_ALT_KEY = "sort.title_alt";
	public static final String SORT_TITLE_ALT_VALUE = "Sort by: Title (Amazon style)";
	
	public static final String SORT_AUTHOR_KEY = "sort.author";
	public static final String SORT_AUTHOR_VALUE = "Sort by: Author";
	
	public static final String SORT_RECENT_KEY = "sort.recent";
	public static final String SORT_RECENT_VALUE = "Sort by: Last access";
	
	public static final String SORT_PUBLICATION_KEY = "sort.publication";
	public static final String SORT_PUBLICATION_VALUE = "Sort by: Publication date";
	
	public static final String SORT_INDEPENDENTLY_KEY = "sort.independently";
	public static final String SORT_INDEPENDENTLY_VALUE = "Sort panels independently";
	
	public static final String PANEL_UPPER_KEY = "panel.upper";
	public static final String PANEL_UPPER_VALUE = "Upper Panel";
	
	public static final String PANEL_LOWER_KEY = "panel.lower";
	public static final String PANEL_LOWER_VALUE = "Lower Panel";
	
	private static URL getLocalizationURL() {
		try {
			File f = new File("/mnt/us/documents/CollectionsManager-localization.txt");
			if (f.exists() && f.isFile() && f.canRead()) {
				return f.toURL();
			}
		} catch (Throwable t) {
		}
		return null;
	}
	
	public static I18n get() {
		return instance;
	}
	
	private final String[] locales;
	private final Map[] translations;
	
	public I18n() {
		this((InputStream) null, null);
	}
	
	public I18n(URL url) {
		this(url, null);
	}
	
	public I18n(URL url, Locale locale) {
		this (safeStream(url), locale);
	}
	
	public I18n(InputStream stream) {
		this(stream, null);
	}
	
	public I18n(InputStream stream, Locale locale) {
		locales = determineLocalesSearchOrder(locale != null ? locale : Locale.getDefault());
		translations = initializeTranslations(locales.length);
		if (stream != null) {
			fillTranslations(stream);
		}
	}

	private static InputStream safeStream(URL url) {
		if (url == null) return null;
		try {
			return url.openStream();
		} catch (IOException e) {
			return null;
		}
	}

	private String[] determineLocalesSearchOrder(Locale current) {
		// we can have at most 3 locales: default, language, and language + country.
		List list = new ArrayList(3);
		list.add(current);
		Locale last = current;
		current = new Locale(last.getLanguage());
		if (!current.equals(last)) {
			list.add(current);
		}
		if (!current.equals(Locale.ENGLISH)) {
			list.add(Locale.ENGLISH);
		}
		
		String[] result = new String[list.size()];
		for (int i=0; i < result.length; ++i) {
			result[i] = ((Locale)list.get(i)).toString();
		}
		return result;
	}

	private Map[] initializeTranslations(int count) {
		Map[] map = new Map[count];
		for (int i=0; i < count; ++i) {
			map[i] = new TreeMap();
		}
		return map;
	}
	
	private void fillTranslations(InputStream stream) {
		BufferedReader r = null;
		try {
			r= new BufferedReader(new InputStreamReader(stream, "UTF-8"));
			Map translation = null;
			for (String line = r.readLine(); line != null; line = r.readLine()) {
				if (isIgnorable(line)) continue;
				String localeCode = interpretAsSection(line);
				if (localeCode != null) {
					translation = lookupTranslationFor(localeCode);
					continue;
				}
				insertIfValid(line, translation);
			}
		} catch (IOException e) {
		} finally {
			if (r != null) {
				try {
					r.close();
				} catch (IOException e) {
				}
			}
		}
	}

	private boolean isIgnorable(String line) {
		String trimmed = line.trim();
		return trimmed.length() == 0 || trimmed.startsWith("#");
	}

	private String interpretAsSection(String line) {
		// allow trailing spaces, but not leading ones
		String trimmed = line.trim();
		if (line.startsWith("[") && trimmed.endsWith("]")) {
			return trimmed.substring(1, trimmed.length()-1);
		}
		return null;
	}

	private Map lookupTranslationFor(String localeCode) {
		for (int i=0; i < locales.length; ++i) {
			if (locales[i].equals(localeCode)) {
				return translations[i];
			}
		}
		return null;
	}

	private void insertIfValid(String line, Map translation) {
		if (translation == null) return;
		int delim = line.indexOf('=');
		if (delim == -1) return;
		String key = line.substring(0, delim).trim();
		String value = line.substring(delim+1).trim();
		translation.put(key, value);
	}

	public String[] getOrderedLocales() {
		String[] copy = new String[locales.length];
		System.arraycopy(locales, 0, copy, 0, locales.length);
		return copy;
	}
	
	public String i18n(String key, String fallback) {
		key = key.trim();
		
		for (int i=0; i < translations.length; ++i) {
			Object match = translations[i].get(key);
			if (match != null) {
				return (String) match;
			}
		}
		return fallback;
	}

	private String getOverrideLocale() {
		return i18n(LOCALE_OVERRIDE, null);
	}
	
	public MessageFormat instantiateFormat(String key,
			String fallback) {
		
		String format = i18n(key, fallback);
		
		String[] localesToTry = locales;
		String overrideLocale= getOverrideLocale();
		if (overrideLocale != null) {
			localesToTry = new String[] {overrideLocale};
		}
		
		StringWriter errors = new StringWriter();
		for (int i=0; i < localesToTry.length; ++i) {
			try {
				Locale jloc = getJLocale(localesToTry[i]);
				ULocale uloc = getULocale(jloc);
				MessageFormat fmt = getMessageFormat(format, uloc);
				return fmt;
			} catch (I18Exception oops) {
				oops.printStackTrace(new PrintWriter(errors));
			}
		}
		throw new RuntimeException(errors.toString());
	}

	private Locale getJLocale(String code) throws I18Exception {
		try {
			return new Locale(code);
		} catch (Throwable t) {
			throw new I18Exception("Unable to instantiate Java locale "+code, t);
		}
	}

	private ULocale getULocale(Locale jloc) throws I18Exception {
		try {
			return ULocale.forLocale(jloc);
		} catch (Throwable t) {
			throw new I18Exception("Unable to instantiate ULocale "+jloc.toString(), t);
		}
	}
	
	private MessageFormat getMessageFormat(String format, ULocale locale) throws I18Exception {
		try {
			return new MessageFormat(format, locale);
		} catch (Throwable t) {
			throw new I18Exception("Unable to instantiate MessageFormat: "+t.getMessage(), t);
		}
	}



	private class I18Exception extends Exception {
		private static final long serialVersionUID = 1L;

		public I18Exception(String message, Throwable cause) {
			super(message, cause);
		}}

}
