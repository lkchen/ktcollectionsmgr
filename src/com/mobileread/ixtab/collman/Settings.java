package com.mobileread.ixtab.collman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import com.amazon.kindle.kindlet.security.SecureStorage;
import com.mobileread.ixtab.collman.adapters.CatalogService;

public class Settings implements ActionListener {

	private static final String COLLECTIONS_ONLY = "collectionsOnly";
	private static final String HIDE_INVISIBLE = "hideInvisible";
	private static final String HIDE_LOWER_PANEL = "hideLowerPanel";
	private static final String FILTER_LOWER_PANEL_ONLY = "filterLowerPanelOnly";
	private static final String SHOW_UNFILED_ONLY = "showUnfiledOnly";
	private static final String SORT_UPPER = "sortUpper";
	private static final String SORT_LOWER = "sortLower";
	private static final String SORT_INDEPENDENTLY = "sortIndependently";
	private static final byte[] FALSE = new byte[] { 0 };
	private static final byte[] TRUE = new byte[] { 1 };

	private static Settings instance;

	private final SecureStorage storage;
	private boolean persistSettings = true;

	public static void init(SecureStorage storage) {
		instance = new Settings(storage);
	}

	public static Settings get() {
		return instance;
	}

	public Settings(SecureStorage storage) {
		this.storage = storage;
		Event.addListener(this);
	}

	public boolean isShowCollectionsOnly() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(COLLECTIONS_ONLY));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isHideInvisibleInHome() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(HIDE_INVISIBLE));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isHideLowerPanel() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(HIDE_LOWER_PANEL));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isFilterLowerPanelOnly() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(FILTER_LOWER_PANEL_ONLY));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isShowUnfiledItemsOnly() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(SHOW_UNFILED_ONLY));
		} catch (Throwable t) {
		}
		return false;
	}
	
	public boolean isSortIndependently() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(SORT_INDEPENDENTLY));
		} catch (Throwable t) {
		}
		return false;
	}
	
	public int getDisplayOrder(int panelPosition) {
		String config = SORT_UPPER;
		if (isSortIndependently() && panelPosition == PanelPosition.LOWER) {
			config = SORT_LOWER;
		}
		try {
			return storage.getBytes(config)[0] & 0xFF;
		} catch (Throwable t) {
			return CatalogService.Sort.TITLE;
		}
	}

	public void actionPerformed(ActionEvent e) {
		onActionPerformed((Event) e);
	}

	private boolean onActionPerformed(Event e) {
		if (!persistSettings) {
			if (e.getID() == Event.SETTINGS_MODE_PERSISTENT) {
				persistSettings = true;
			}
			return true;
		} else {
			switch (e.getID()) {
			case Event.SETTINGS_MODE_EPHEMERAL:
				persistSettings = false;
				return true;
			case Event.DISPLAY_ALL:
				return set(COLLECTIONS_ONLY, FALSE);
			case Event.DISPLAY_COLLECTIONS_ONLY:
				return set(COLLECTIONS_ONLY, TRUE);
			case Event.INVISIBLE_HIDE:
				return set(HIDE_INVISIBLE, TRUE);
			case Event.INVISIBLE_SHOW:
				return set(HIDE_INVISIBLE, FALSE);
			case Event.LOWER_PANEL_DISABLE:
				return set(HIDE_LOWER_PANEL, TRUE);
			case Event.LOWER_PANEL_ENABLE:
				return set(HIDE_LOWER_PANEL, FALSE);
			case Event.FILTER_LOWER_ONLY_ON:
				return set(FILTER_LOWER_PANEL_ONLY, TRUE);
			case Event.FILTER_LOWER_ONLY_OFF:
				return set(FILTER_LOWER_PANEL_ONLY, FALSE);
			case Event.SHOW_UNFILED_ONLY_ON:
				return set(SHOW_UNFILED_ONLY, TRUE);
			case Event.SHOW_UNFILED_ONLY_OFF:
				return set(SHOW_UNFILED_ONLY, FALSE);
			case Event.SORT_INDEPENDENTLY_ON:
				return set(SORT_INDEPENDENTLY, TRUE);
			case Event.SORT_INDEPENDENTLY_OFF:
				return set(SORT_INDEPENDENTLY, FALSE);
			case Event.SORT_TITLE:
				return sortChanged(CatalogService.Sort.TITLE, PanelPosition.UPPER);
			case Event.SORT_TITLE_ALT:
				return sortChanged(CatalogService.Sort.TITLE_ALT, PanelPosition.UPPER);
			case Event.SORT_AUTHOR:
				return sortChanged(CatalogService.Sort.AUTHOR, PanelPosition.UPPER);
			case Event.SORT_RECENT:
				return sortChanged(CatalogService.Sort.RECENT, PanelPosition.UPPER);
			case Event.SORT_PUBLICATION:
				return sortChanged(CatalogService.Sort.PUBLICATION, PanelPosition.UPPER);
			case Event.SORT_TITLE_LOWER:
				return sortChanged(CatalogService.Sort.TITLE, PanelPosition.LOWER);
			case Event.SORT_TITLE_ALT_LOWER:
				return sortChanged(CatalogService.Sort.TITLE_ALT, PanelPosition.LOWER);
			case Event.SORT_AUTHOR_LOWER:
				return sortChanged(CatalogService.Sort.AUTHOR, PanelPosition.LOWER);
			case Event.SORT_RECENT_LOWER:
				return sortChanged(CatalogService.Sort.RECENT, PanelPosition.LOWER);
			case Event.SORT_PUBLICATION_LOWER:
				return sortChanged(CatalogService.Sort.PUBLICATION, PanelPosition.LOWER);
			default:
				return false;
			}
		}
	}
	
	private boolean sortChanged(int newSort, int panelPosition) {
		String config = SORT_UPPER;
		if (isSortIndependently() && panelPosition == PanelPosition.LOWER) {
			config = SORT_LOWER;
		}
		boolean result = set(config, newSort);
		CatalogService.Sort.set(newSort, panelPosition);
		Event.post(Event.SORT_CHANGED, this, PanelPosition.BOTH);
		return result;
	}

	private boolean set(String key, int value) {
		return set(key, new byte[] {(byte)value});
	}
	
	private boolean set(String key, byte[] value) {
		try {
			storage.putBytes(key, value);
		} catch (Throwable t) {
			return false;
		}
		return true;
	}
}
