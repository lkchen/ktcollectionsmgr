package com.mobileread.ixtab.collman.adapters.fw5442;

import java.lang.reflect.Field;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.booklet.B;
import com.amazon.kindle.booklet.g;
import com.amazon.kindle.booklet.k;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.fw5442.SearchHandler5442;

public class CollectionManager5442 extends CollectionsManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager5442(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
		try {
			com.amazon.kindle.kindlet.internal.ui.k x = (com.amazon.kindle.kindlet.internal.ui.k) context.getService(com.amazon.kindle.kindlet.ui.Toolbar.class);
			Field f = x.getClass().getDeclaredField("M"); //ChromeHeaderRequest??
			f.setAccessible(true);

			f = x.getClass().getDeclaredField("g"); //BookletContext??
			f.setAccessible(true);
			return (BookletContext) f.get(x);
		} catch (Throwable t) {
			return null;
		}
	}

	protected void modifyToolbar() {
		if (chromeImplementation == null || bookletContext == null) {
			return;
		}

		boolean registerSearch = true;
		try {
			chromeImplementation.ujb(bookletContext, "default", SearchHandler5442
					.getInstance5442().getChromeSearchProvider());
		} catch (B e) {
			registerSearch = false;
		}
		if (registerSearch) {
			SearchHandler5442.getInstance5442().register(bookletContext);
		}

		try {
			k sbr = new k(
					"com.lab126.booklet.kindlet");
			sbr.Shc("default",
					new g[] {
							new g("forward", "invisible",
									"system"),
							new g("back", "invisible",
									"system"), });
			chromeImplementation.fJ(bookletContext, sbr, true);
		} catch (B e) {
			// this is actually known to throw an exception, yet it still
			// provides the wanted result. Go figure.
		}
		}

	public void onStop() {
		SearchHandler5442.getInstance5442().unregister(bookletContext);
	}

	protected boolean isSearchSupported() {
		return true;
	}

}
