package com.mobileread.ixtab.collman.adapters.fw512;

import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.booklet.ChromeSearchProvider;
import com.amazon.kindle.control.resources.MenuResources;
import com.amazon.kindle.util.lipc.LipcException;
import com.amazon.kindle.util.lipc.LipcSource;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class SearchHandler512 extends SearchHandler {

	private static final String LIPC_PROPERTY = "searchSelection";

	private static final String searchLabel = getLocalizedLabel();

	private static String getLocalizedLabel() {
		ResourceBundle bundle = ResourceBundle.getBundle(MenuResources.class.getName());
		StringBuffer sb = new StringBuffer(bundle.getString("sort.title.label"));
		sb.append(" / ");
		sb.append(bundle.getString("sort.author.label"));
		return sb.toString();
	}

	public static SearchHandler512 getInstance512() {
		return (SearchHandler512)com.mobileread.ixtab.collman.adapters.SearchHandler.instance;
	}

	public SearchHandler512() {
		Event.addListener(this);
	}

	public ChromeSearchProvider getChromeSearchProvider() {
		return new ChromeSearchProvider("search", searchLabel, "", Boolean.TRUE);
	}

	public void register(BookletContext bookletContext) {
		if (bookletContext != null) {
			try {
				LipcSource lipc = bookletContext.getBookletSource();
				if (lipc.isPropertyExported(LIPC_PROPERTY)) {
					lipc.removeExportedProperty(LIPC_PROPERTY);
				}
				lipc.exportStringProperty(LIPC_PROPERTY, this, 2);
			} catch (Exception e) {}
		}
	}

	public void unregister(BookletContext bookletContext) {
		if (bookletContext != null) {
			try {
				LipcSource lipc = bookletContext.getBookletSource();
				if (lipc.isPropertyExported(LIPC_PROPERTY)) {
					lipc.removeExportedProperty(LIPC_PROPERTY);
				}
			} catch (Throwable t) {}
		}
	}

	public void setProperty(String key, String value) throws LipcException {
		JSONObject json = (JSONObject) JSONValue.parse(value);
		String search = (String) json.get("label");
		search = search.trim();
		updateQuery(search);
	}

	private void updateQuery(String search) {
		query = search;
		Event.post(Event.SEARCH, this, PanelPosition.BOTH);
	}

	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (!e.appliesTo(PanelPosition.UPPER)) {
			return;
		}
		if (e.getID() == Event.PATH_ENTRY_CLICKED || e.getID() == Event.VIEW_ENTRY_CLICKED) {
			if (query.length() > 0) {
				updateQuery("");
			}
		}
	}

	private String query = "";
	public String getSearchQuery() {
		return query;
	}

}
