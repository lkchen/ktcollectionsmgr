package com.mobileread.ixtab.collman.catalog;

import java.util.ArrayList;
import java.util.List;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;

public abstract class Collection extends Entry {

	protected boolean isCollection() {
		return true;
	}

	public CatalogEntryCollection getBackendCollection() {
		return (CatalogEntryCollection) getBackend();
	}
	
	private final int[] totalSize = new int[1];
	
	public Entry[] getEntries(Predicate filter, int offset, int count) {
		return getEntries(PanelPosition.BOTH, filter, offset, count);
	}
	
	public Entry[] getEntries(int panel, Predicate filter, int offset, int count) {
		Entry[] result = new Entry[count];
		Predicate pred = Catalog.Predicates.ALL_ENTRIES;
		if (!isRoot()) {
			Object[] members = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
			if (members == null) {
				members = new Object[0];
			}
			pred = PredicateFactory.inList("uuid", members);
		}
		if (filter != null) {
			pred = PredicateFactory.and(new Predicate[] {
					pred,
					filter,
			});
		}
		CatalogEntry[] ces = Catalog.find(panel, pred, true, offset, count, totalSize);
		for (int i=0; i < ces.length; ++i) {
			result[i] = Entry.instantiate(ces[i]);
		}
		return result;
	}

	public boolean addEntry(Entry child) {
		if (child.getUuid().equals(getUuid())) {
			return false;
		}
		
		if (!isRoot()) {
			Object[] members = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
			MutableCollection mutable = Catalog.createMutableCollection(this);
			for (int i=0; i < members.length; ++i) {
				CatalogAdapter.INSTANCE.addMember(mutable, members[i]);
				if (members[i].equals(child.getUuid())) {
					return true;
				}
			}
			CatalogAdapter.INSTANCE.addMember(mutable, child.getUuid());
			Catalog.update(mutable);
		}
		reload();
		childUpdated(child);
		return true;
	}

	public void removeEntry(Entry entry) {
		if (!isRoot() && Entry.equals(entry, this)) {
			// why on earth did I write this code? :-D
			return;
		}
		if (isRoot()) {
			entry.setVisible(false);
		} else {
			Object[] currentMembers = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
			List newMembers = new ArrayList();
			for (int i=0; i < currentMembers.length; ++i) {
				Object uuid = currentMembers[i];
				if (!uuid.equals(entry.getUuid())) {
					newMembers.add(uuid);
				}
			}
			if (newMembers.size() == currentMembers.length) {
				return;
			}
			MutableCollection mutable = Catalog.createMutableCollection(this);
			Object[] nma = new Object[newMembers.size()];
			for (int i= 0; i < nma.length; ++i) {
				nma[i] = newMembers.get(i);
			}
			CatalogAdapter.INSTANCE.setMembers(mutable, nma);
			Catalog.update(mutable);
		}
		reload();
		childUpdated(entry);

	}

	private void childUpdated(Entry child) {
		child.reload();
		if (CatalogAdapter.INSTANCE.countParents(child.getBackend()) == 0 && !child.isVisible()) {
			child.setVisible(true);
		}
	}

	public void renameTo(String newTitle) {
		Catalog.getCache().set(getUuid(), newTitle);
		MutableEntry mutable = Catalog.createMutableEntry(this);
		CatalogAdapter.INSTANCE.setTitle(mutable, newTitle);
		Catalog.update(mutable);
		Object[] parents = CatalogAdapter.INSTANCE.getParents(getBackend());
		for (int i=0; i < parents.length; ++i) {
			Entry.instantiate(Catalog.load(parents[i])).reload();
		}
		reload();
		
	}

	public boolean isEmpty() {
		return CatalogAdapter.INSTANCE.getMembers(getBackendCollection()).length == 0;
	}

	public void delete() {
		
		Object[] children = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
		Object[] parents = CatalogAdapter.INSTANCE.getParents(getBackend());
		Catalog.getCache().delete(getUuid());
		Catalog.delete(getUuid());
		Event.post(Event.COLLECTION_DELETED, this, PanelPosition.BOTH);
		
		for (int i=0; i < parents.length; ++i) {
			CatalogEntry entry = Catalog.load(parents[i]);
			if (entry != null) {
				Entry.instantiate(entry).reload();
			}
		}
		for (int i=0; i < children.length; ++i) {
			CatalogEntry entry = Catalog.load(children[i]);
			if (entry != null) {
				childUpdated(Entry.instantiate(entry));
			}
		}
	}

	public int getSize() {
		if (isRoot()) {
			return totalSize[0];
		}
		return CatalogAdapter.INSTANCE.countMembers(getBackendCollection());
	}

	public CatalogEntry getBackend() {
		return null;
	}

	protected void setBackend(CatalogEntry update) {
	}

	public String toString() {
		return super.toString() + " ["+getSize()+"]";
	}

	public int hashCode() {
		return toString().hashCode();
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Collection other = (Collection) obj;
		return other.toString().equals(this.toString());
	}

	
	
}
