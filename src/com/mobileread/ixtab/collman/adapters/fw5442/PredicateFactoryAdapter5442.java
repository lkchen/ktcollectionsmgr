package com.mobileread.ixtab.collman.adapters.fw5442;

import com.amazon.kindle.content.catalog.G;
import com.amazon.kindle.content.catalog.h;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter5442 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private h[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		h[] out = new h[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (h) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(G.dIc(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(G.PHc(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(G.Thc((h) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(G.XGc(key));
	}

	public Predicate isTrue(String what) {
		return wrap(G.ygc(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(G.XNb(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(G.xHc(key, CatalogAdapter5442
                .asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(G.ajc(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(G.WIc(key, value));
	}

}
