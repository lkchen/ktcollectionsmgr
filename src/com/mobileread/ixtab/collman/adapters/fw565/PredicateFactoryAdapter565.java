package com.mobileread.ixtab.collman.adapters.fw565;

import com.amazon.kindle.content.catalog.B;
import com.amazon.kindle.content.catalog.L;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter565 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private L[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		L[] out = new L[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (L) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(B.jgC(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(B.dhC(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(B.JGC((L) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(B.sgC(key));
	}

	public Predicate isTrue(String what) {
		return wrap(B.KhC(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(B.TFB(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(B.uIC(key, CatalogAdapter565
                .asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(B.OHC(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(B.AiC(key, value));
	}

}
