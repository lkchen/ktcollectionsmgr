package com.mobileread.ixtab.collman.ui.menu.display;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Settings;
import com.mobileread.ixtab.collman.actions.ToggleShowUnfiledItemsOnlyAction;

public class ShowUnfiledItemsOnlyButton extends JCheckBox implements ActionListener {
	private static final long serialVersionUID = 1L;

	public ShowUnfiledItemsOnlyButton() {
		super(new ToggleShowUnfiledItemsOnlyAction());
		setSelected(Settings.get().isShowUnfiledItemsOnly());
		Event.addListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (e.getID() == Event.SHOW_UNFILED_ONLY_OFF) {
			setSelected(false);
		} else if (e.getID() == Event.SHOW_UNFILED_ONLY_ON) {
			setSelected(true);
		}
	}
}

