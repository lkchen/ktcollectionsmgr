package com.mobileread.ixtab.collman.adapters.fw545;

import com.amazon.ebook.util.a.h;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction545 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction545(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.vJ((h) uuid);
	}

	public boolean commitSync() {
		return delegate.Nm().YIc();
	}

	public void addEntry(MutableEntry c) {
		delegate.Ok(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.hJ(entry);
	}

}
