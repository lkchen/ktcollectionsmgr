package com.mobileread.ixtab.collman.adapters.fw539;

import com.amazon.kindle.content.catalog.b;
import com.amazon.kindle.content.catalog.g;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter539 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private g[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i = 0; i < in.length; ++i) {
			out[i] = (g) in[i].delegate;
		}
		return out;
	}

	/*
	 * NOTE: Line numbers indicate where in the jadclipse-generated file the
	 * method starts. This isn't really useful *here*, but it will be useful for
	 * adapting future firmwares, which will inevitably have a different
	 * obfuscation scheme yet again. Consider these approximate, as that may
	 * also depend on the jadclipse setup etc. But these are the numbers on my
	 * system.
	 * 
	 * Oh, did I already say "FUCK YOU, Lab126"?
	 */

	// Line 19
	public Predicate and(Predicate[] predicates) {
		return wrap(b.edB(explode(predicates)));
	}

	// 24
	public Predicate or(Predicate[] predicates) {
		return wrap(b.HbB(explode(predicates)));
	}

	// 29
	public Predicate not(Predicate pred) {
		return wrap(b.AdB((g) pred.delegate));
	}

	// 34
	public Predicate notNull(String key) {
		return wrap(b.CCB(key));
	}

	// 47
	public Predicate isTrue(String what) {
		return wrap(b.BBB(what));
	}

	// 81
	public Predicate equals(String key, String value) {
		return wrap(b.vwA(key, value));
	}

	// 129
	public Predicate inList(String key, Object membersArray) {
		return wrap(b.abB(key, CatalogAdapter539
				.asUUIDArray((Object[]) membersArray)));
	}

	// 252
	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(b.HdB(key, value, inclusive));
	}

	// 300
	public Predicate startsWith(String key, String value) {
		return wrap(b.DcB(key, value));
	}

}
