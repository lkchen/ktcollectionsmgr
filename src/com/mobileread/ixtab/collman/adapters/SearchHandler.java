package com.mobileread.ixtab.collman.adapters;

import java.awt.event.ActionListener;
import com.amazon.kindle.util.lipc.LipcPropertyAdapter;

public abstract class SearchHandler extends LipcPropertyAdapter implements ActionListener {

	public static final SearchHandler instance = AdapterConfiguration
			.getInstance().getSearchHandler();

	public static SearchHandler getInstance() {return instance;};

	public abstract String getSearchQuery();

}
