package com.mobileread.ixtab.collman.ui.menu.sort;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Settings;
import com.mobileread.ixtab.collman.actions.SortAction;

public class SortButton extends JRadioButton implements ActionListener {
	private static final long serialVersionUID = 1L;

	private final int ownEventId;
	private final int groupMinEvent;
	private final int groupMaxEvent;

	public SortButton(String label, int sort, int ownEvent, int panelPosition, int minEvent, int maxEvent) {
		super(new SortAction(label, ownEvent));
		this.ownEventId = ownEvent;
		this.groupMinEvent = minEvent;
		this.groupMaxEvent = maxEvent;
		setSelected(sort == Settings.get().getDisplayOrder(panelPosition));
		Event.addListener(this);
	}

	public void actionPerformed(ActionEvent event) {
		int id = ((Event) event).getID();
		if (id >= groupMinEvent && id <= groupMaxEvent) {
			handleSortEvent(id);
		}
	}

	private void handleSortEvent(int event) {
		setSelected(ownEventId == event);
		this.invalidate();
		this.repaint();
	}
}
