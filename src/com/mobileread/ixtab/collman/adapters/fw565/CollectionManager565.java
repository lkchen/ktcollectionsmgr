package com.mobileread.ixtab.collman.adapters.fw565;

import java.lang.reflect.Field;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.booklet.g;
import com.amazon.kindle.booklet.j;
import com.amazon.kindle.booklet.l;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;

public class CollectionManager565 extends CollectionsManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager565(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
		try {
			com.amazon.kindle.kindlet.internal.ui.E x = (com.amazon.kindle.kindlet.internal.ui.E) context.getService(com.amazon.kindle.kindlet.ui.Toolbar.class);
			Field f = x.getClass().getDeclaredField("M"); //ChromeHeaderRequest??
			f.setAccessible(true);

			f = x.getClass().getDeclaredField("g"); //BookletContext??
			f.setAccessible(true);
			return (BookletContext) f.get(x);
		} catch (Throwable t) {
			return null;
		}
	}

	protected void modifyToolbar() {
		if (chromeImplementation == null || bookletContext == null) {
			return;
		}

		boolean registerSearch = true;
		try {
			chromeImplementation.PCB(bookletContext, "default", SearchHandler565
					.getInstance565().getChromeSearchProvider());
		} catch (j e) {
			registerSearch = false;
		}
		if (registerSearch) {
			SearchHandler565.getInstance565().register(bookletContext);
		}

		try {
			g sbr = new g(
					"com.lab126.booklet.kindlet");
			sbr.tJC("default",
					new l[] {
							new l("forward", "invisible",
									"system"),
							new l("back", "invisible",
									"system"), });
			chromeImplementation.iL(bookletContext, sbr, true);
		} catch (j e) {
			// this is actually known to throw an exception, yet it still
			// provides the wanted result. Go figure.
		}
		}

	public void onStop() {
		SearchHandler565.getInstance565().unregister(bookletContext);
	}

	protected boolean isSearchSupported() {
		return true;
	}

}
